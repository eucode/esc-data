var fs = require('fs');
var path = require('path');

module.exports = plugin;

function plugin (origin) {
    origin = origin || "./src"
    //options = options || {};
    //var keys = options.keys || [];

    return function (files, metalsmith, done){
        var dest_path = "./partials/nav.hbs";

        var data = dirsToHTML(origin, metalsmith._source)

        data = `<nav>\n\t<ul>\n\t\t${data}\n\t</ul>\n</nav>\n`;

        fs.writeFile(dest_path, data, (err) => {
            if (err) throw err;
            console.log("folder-nav: NAV created!");
        });

        done();
    };
}

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
}

function dirsToHTML(dir,source) {
    var dirs = getDirectories(dir)
    var data = ""
    for (var d of dirs) {
        var dth = dirsToHTML(dir+'/'+d)
        var Dir = firstLetterUpperCase(d)
        var output = dir.replace(source, ".")
        data += `<li><a href='${output+'/'+d}'>${Dir}</a>`
        if (dth !== "") data += `\n<ul>\n${dth}\n<ul>\n`;
        data += '</li>'
    };
    return data;
}

function firstLetterUpperCase(str) {
    var spl = str.split('_');
    for (var i = 0; i < spl.length; i++) {
        spl[i] = spl[i][0].toUpperCase()+spl[i].slice(1);
    }
    return spl.join(" ");
}
