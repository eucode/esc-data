var Metalsmith = require("metalsmith");
var markdown = require('metalsmith-markdown');
var folder_nav = require('./lib/folder-nav')
var layouts = require('metalsmith-layouts');

Metalsmith(__dirname)
    .source("./src")
    .destination("./public")
    .use(markdown())
    .use(folder_nav("./src/api"))
    .use(layouts({
        engine: "handlebars",
        partials: "partials"
    }))
    .build(err => { if (err) throw err; })
